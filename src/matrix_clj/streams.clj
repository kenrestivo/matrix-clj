(ns matrix-clj.streams
  "Functions for processing Matrix event streams"
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [clj-http.util :as hutil]
            [cheshire.core :as json]
            [clj-http.client :as client]))




(defn event-contains?
  "GIven a lower-case target string and a matrix event,
  returns true if the body of the event contains the (case-insensitive) target string"
  [target ev]
  (when-let [s (some-> ev :content :body)]
    (and (string? s)
         (-> s
             .toLowerCase
             (.contains  target)))))


(defn count-mentions
  "Given an event stream and a bot name (lower case),
  returns the count of case-insensitive mentions of bot name"
  [bot-name evs]
  (some->> evs
           :timeline
           :events
           (filter (partial event-contains? (.toLowerCase bot-name)))
           count))


(defn is-invite?
  "Given a matrix event, returns true if it is an invite event"
  [ev]
  (some-> ev
          :content
          :membership
          (= "invite")))

(defn count-invites
  "Given an event stream, returns the count of invites
   (Presuming only invites to this bot will show up in the stream!)"
  [evs]
  (some->> evs
           :invite_state
           :events
           (filter is-invite?)
           count))


(defn rooms-matching-filter
  "Given an event type, a function to filter on that takes an event stream, and a stream,
  returns the names of the rooms that match the filter."
  [event-type filter-fn stream]
  (->> (for [[room evs] (some-> stream
                                :rooms
                                event-type)]
         [room (filter-fn evs)])
       (filter (comp pos? second))
       (map first)
       (map name)))



(def stream->invites
  (partial rooms-matching-filter :invite count-invites))


(defn stream->mentions
  "Takes a matrix stream, returns a seq of rooms with mentions matching a bot name."
  [bot-name stream]
  (rooms-matching-filter :join (partial count-mentions bot-name) stream))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  )
