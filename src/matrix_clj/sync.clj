(ns matrix-clj.sync
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [taoensso.timbre :as log]
            [matrix-clj.net :as net]
            [clojure.edn :as edn]
            [clj-http.util :as hutil]
            [cheshire.core :as json]
            [clj-http.client :as client]))




(defn run-sync*
  "Takes a token, base-url to the matrix server, timout in milliseconds, 
    an initial sync timestamp in matrix format, and a function to run on every stream returned from sync, 
    which takes a token, base-url and the stream map.
    This is essentially the main loop, does not exit."
  [token base-url timeout initial-sync f]
  (log/info "starting loop" base-url timeout initial-sync f)
  (loop [nb initial-sync]
    (log/trace "next loop" nb)
    (let [{:keys [next_batch] :as stream} (ulog/catcher
                                           (:body (net/sync-stream token base-url timeout nb)))]
      (log/trace "next batch is:" next_batch)
      (ulog/catcher
       (f token base-url stream))
      (recur (or next_batch nb)))))

(defn run-sync
  "Takes a token, base-url to the matrix server, timout in milliseconds, 
    and a function to run on every stream returned from sync, 
    (which takes a token, base-url and the stream map). Gets initial sync,
    calls the main loop, and does not exit."
  [token base-url timeout f]
  (log/info "getting initial sync")
  (let [isync (-> (net/initial-sync token base-url timeout)
                  :body
                  :next_batch)]
    (log/debug "initial sync found:" isync)
    (run-sync* token base-url timeout isync f)))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  )
