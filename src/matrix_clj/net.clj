(ns matrix-clj.net
  "Interfacing with the Matrix API via HTTPS"
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [clj-http.util :as hutil]
            [cheshire.core :as json]
            [clj-http.client :as client]))



(defn initial-sync
  "Given a token, a base-url and a timeout in milliseconds,
  Returns the initial sync (oldest event since last sync from this token's login)"
  [token base-url timeout-ms]
  (client/get (format "%s/_matrix/client/r0/sync" base-url) 
              {:content-type :json
               :as :json
               :accept :json
               :query-params {:filter (json/encode {:room {:timeline {:limit 1}}})  
                              :timeout timeout-ms}
               :headers {"Authorization" (str "Bearer " token)}
               :throw-exceptions false
               :form-params {}}))

(defn sync-stream
  "Takes token, matrix base url, a timeout in milliseconds, 
   and a since (next_batch) stamp in matrix format.
   Returns a stream of events from matrix"
  [token base-url timeout-ms since]
  (client/get (format "%s/_matrix/client/r0/sync" base-url) 
              {:content-type :json
               :as :json
               :accept :json
               :query-params {:since since
                              :timeout timeout-ms}
               :headers {"Authorization" (str "Bearer " token)}
               :throw-exceptions false
               :form-params {}}))


(defn join-room!
  "Given a token, base url, and room id, attempts to join it."
  [token base-url rheum-id ]
  (log/debug "joining" base-url rheum-id)
  (client/post (format "%s/_matrix/client/r0/rooms/%s/join"
                       base-url
                       (hutil/url-encode rheum-id)) 
               {:content-type :json
                :as :json
                :accept :json
                :headers {"Authorization" (str "Bearer " token)}
                :throw-exceptions false
                :form-params {}}))



(defn send-message!
  "Takes a token, matrix server base url, a room  id, and the text of the message.
   Sends it to that room. Returns whatever matrix replies."
  [token base-url   rheum-id  msg]
  (log/trace "sending" base-url rheum-id msg)
  (client/post (format "%s/_matrix/client/r0/rooms/%s/send/m.room.message"
                       base-url
                       (hutil/url-encode rheum-id)) 
               {:content-type :json
                :as :json
                :accept :json
                :headers {"Authorization" (str "Bearer " token)}
                :throw-exceptions false
                :form-params {:msgtype "m.text"
                              :body msg}}))



(defn login
  "Given an auth map (base-url, username, passwrod, device-id),
   logs into Matrix server at base-url, and returns an access token macaroon and tx-id 1"
  [{:keys [base-url username password device-id]}]
  {:token (some->> (client/post (str base-url "/_matrix/client/r0/login")
                                {:as :json
                                 :form-params (cond-> {:type "m.login.password"
                                                       :password password,
                                                       :user username}
                                                device-id (assoc :device_id device-id
                                                                 :initial_device_display_name device-id)) 
                                 :content-type :json
                                 ;; TODO: retry?
                                 :throw-exceptions true})
                   :body
                   :access_token)
   :tx-id 1})


(defn logout!
  "Given a base-url and a macaroon token,
  logs this macarroon/token/session out of Matrix.
  The token will be invalid forever afterwards."
  [{:keys [url token]}]
  (log/info "logging out of" url )
  (-> (str url "/_matrix/client/r0/logout")
      (client/post {:as :json
                    :form-params {}
                    :query-params {:access_token token}
                    :content-type :json
                    ;; TODO: retry?
                    :throw-exceptions true})
      :body))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  )
