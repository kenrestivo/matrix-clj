# Matrix-CLJ

Useful functions for interfacing with the [Matrix protocol](https://matrix.org/docs/spec/)

## Installation

Import with Lein:

```clojure
[matrix-clj "0.1.0"]
```

## Examples

The [Cthulhubot](https://gitlablcom/kenrestivo/cthulhubot) uses this library.

## License

Copyright © 2018 ken restivo <ken@restivo.org>

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
