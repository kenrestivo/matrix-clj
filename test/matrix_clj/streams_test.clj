(ns matrix-clj.streams-test
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [clojure.test :refer :all]
            [matrix-clj.streams :refer :all]
            [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [clj-http.util :as hutil]
            [cheshire.core :as json]
            [clj-http.client :as client]))


(deftest invite-read-test
  (is (= (->> "test-data/messages.edn"
              slurp
              edn/read-string
              stream->invites)
         []))
  (is (= (->> "test-data/invite.edn"
              slurp
              edn/read-string
              stream->invites)
         ["!gixCzAQTwtTcNLVtuH:spaz.org"])))

(deftest mention-read-test
  (is (= (->> "test-data/invite.edn"
              slurp
              edn/read-string
              (stream->mentions "cthulhu"))
         []))
  (is (= (->> "test-data/messages.edn"
              slurp
              edn/read-string
              (stream->mentions "cthulhu"))
         ["!gixCzAQTwtTcNLVtuH:spaz.org"])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (ulog/catcher
   (run-tests))

  
  
  )
