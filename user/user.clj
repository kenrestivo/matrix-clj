(ns user
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [taoensso.timbre :as log]
            [matrix-clj.matrix :as m]
            [clojure.edn :as edn]
            [matrix-clj.net :as net]
            [clojure.java.io :as jio]
            [clj-http.util :as hutil]
            [cheshire.core :as json]
            [clj-http.client :as client]))


(comment


  (ulog/spewer
   (let [{:keys [base-url timeout creds]} (->> "/home/cust/spaz/keys/cthulhu.edn"
                                               slurp
                                               edn/read-string)
         {:keys [access_token]} creds]
     (->> (net/initial-sync access_token base-url timeout)
          :body
          :rooms
          :join
          keys
          (map name))))



  (ulog/spewer
   (-> "/home/cust/spaz/keys/cthulhu-login.edn"
       slurp
       edn/read-string
       net/login))


  (ulog/spewer
   (-> "test-data/initial.edn"
       slurp
       edn/read-string))
  
  
  )

