# Change Log
All notable changes to this project will be documented in this file.
This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).


## 0.1.0 - 2018-09-19
### Added
- Initial release

[Unreleased]: https://gitlab.com/kenrestivo/matrix-clj/compare/0.1.1...HEAD
[0.1.1]: https://gitlab.com/kenrestivo/matrix-clj/compare/0.1.0...0.1.1
