(defproject matrix-clj "0.1.3"
  :description "Matrix protocol functions"
  :url "https://gitlab.com/kenrestivo/matrix-clj"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [clj-http "3.9.1"]
                 [utilza "0.1.98"]
                 [com.taoensso/timbre "4.10.0"]
                 [clj-time "0.14.4"]
                 [cheshire "5.8.0"]]
  :main ^:skip-aot matrix-clj.matrix
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:plugins [[lein-difftest "2.0.0"]
                             [lein-codox "0.10.5"]]}})
